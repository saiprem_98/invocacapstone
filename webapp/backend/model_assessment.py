from simpletransformers.classification import ClassificationModel
from sklearn.pipeline import Pipeline
import pickle
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import time
import pandas as pd
import spacy
from predictions import serve_prediction
import os

os.environ["TOKENIZERS_PARALLELISM"] = "false"
def data_process(input_data):
    text = []
    labels = []

    for utterance in input_data['utterances']:
        text_merge = ''
        for line in utterance:
            if line['speaker'] == 'USER':
                text_merge += ' '
                text_merge += line['text']
            if 'segments' in line.keys():
                current_label = line['segments'][0]['annotations'][0]['name'].split(".")[0]
                if current_label:
                    label = current_label
                else:
                    continue
        labels.append(label)
        text.append(text_merge)

    data = pd.DataFrame(list(zip(text, labels)), columns=['text', 'label'])

    cleanup_nums = {"label": {"restaurant_reservation": 1, "movie_ticket": 2, "pizza_ordering": 3,
                              "coffee_ordering": 4, "auto_repair": 5, "uber_lyft": 6}}

    data = data.replace(cleanup_nums)

    return data

def main():
    nlp = spacy.load('en_core_web_sm')
    woz_path = "https://raw.githubusercontent.com/google-research-datasets/Taskmaster/master/TM-1-2019/woz-dialogs.json"
#     solo_path = "https://raw.githubusercontent.com/google-research-datasets/Taskmaster/master/TM-1-2019/self-dialogs.json"

#     solo_data = pd.read_json(solo_path)
    woz_data = pd.read_json(woz_path)
    data = data_process(woz_data)

    bert = ClassificationModel('bert', 'outputs/best_model', num_labels=6, use_cuda=False)
    with open('clf_pipeline', 'rb') as out_file:
        logistic_pipe = pickle.load(out_file)


    log_preds = list()
    bert_preds = list()
    y_true = data.label.to_list()

    for index, text, label in data.itertuples():
        bert_pred = serve_prediction(text, nlp, bert, 'bert')
        bert_preds.append(bert_pred)
        log_pred = serve_prediction(text, nlp, logistic_pipe, 'log')
        log_preds.append(log_pred)

    bert_comparison = [i==j for i, j in zip(bert_pred, y_true)]
    log_comparison = [i==j for i, j in zip(log_pred, y_true)]

    data['Bert_pred'] = bert_preds
    data['Bert_isright'] = bert_comparison
    data['Log_pred'] = log_preds
    data['Log_isright'] = log_comparison

    comparison_data = data.loc[(data['Bert_isright'] == False) or (data['log_isright'] == False)]
    comparison_data.to_csv('model_comparison')






if __name__=='__main__':
    main()