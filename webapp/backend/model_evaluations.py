from simpletransformers.classification import ClassificationModel
from sklearn.pipeline import Pipeline
import pickle
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import time
import pandas as pd
import spacy
from predictions import serve_prediction
import os
os.environ["TOKENIZERS_PARALLELISM"] = "false"

def main():
    nlp = spacy.load('en_core_web_sm')
    timings = list()
    woz_path = "https://raw.githubusercontent.com/google-research-datasets/Taskmaster/master/TM-1-2019/woz-dialogs.json"
    solo_path = "https://raw.githubusercontent.com/google-research-datasets/Taskmaster/master/TM-1-2019/self-dialogs.json"

    solo_data = pd.read_json(solo_path)
    woz_data = pd.read_json(woz_path)
    def data_process(input_data):

        text = []
        labels = []

        for utterance in input_data['utterances']:
            text_merge = ''
            for line in utterance:
                if line['speaker'] == 'USER':
                    text_merge += ' '
                    text_merge += line['text']
                if 'segments' in line.keys():
                    current_label = line['segments'][0]['annotations'][0]['name'].split(".")[0]
                    if current_label:
                        label = current_label
                    else:
                        continue
            labels.append(label)
            text.append(text_merge)

        data = pd.DataFrame(list(zip(text, labels)), columns=['text', 'label'])

        cleanup_nums = {"label": {"restaurant_reservation": 1, "movie_ticket": 2, "pizza_ordering": 3,
                                  "coffee_ordering": 4, "auto_repair": 5, "uber_lyft": 6}}

        data = data.replace(cleanup_nums)

        return data

    data = data_process(woz_data)[:100]

    data_length = len(data)

    # for i in range(100):
    #     time_start = time.time()
    #     model = ClassificationModel('bert', 'outputs/best_model', num_labels=6, use_cuda=False)
    #     print(model)
    #     time_end = time.time()
    #     duration = time_end - time_start
    #     timings.append(duration)

    pred_time = list()
    model = ClassificationModel('bert', 'outputs/best_model', num_labels=6, use_cuda=False)
    for index, text, label in data.itertuples():
        time_start = time.time()
        serve_prediction(text, nlp, model, 'bert')
        time_end = time.time()
        duration = time_end - time_start
        pred_time.append(duration)


    #loading_durations = np.array(timings)
    pred_durations = np.array(pred_time)
    model_eval_text = str()
    # model_eval_text += 'BERT evaluation'
    # model_eval_text += 'Mean duration of loading: ' + str(np.mean(loading_durations)) + '\n\n'
    # model_eval_text += 'Standard deviation of loading: ' + str(np.std(loading_durations)) + '\n\n'
    # model_eval_text += 'Max loading time: ' + str(max(loading_durations)) + '\n\n'
    # model_eval_text += 'Min loading time: ' + str(min(loading_durations)) + '\n\n\n\n'
    model_eval_text += 'Mean duration of prediction: ' + str(np.mean(pred_durations))
    model_eval_text += 'Mean duration of prediction: ' + str(np.mean(pred_durations)) + '\n\n'
    model_eval_text += 'Standard deviation of prediction: ' + str(np.std(pred_durations)) + '\n\n'
    model_eval_text += 'Max prediction time: ' + str(max(pred_durations)) + '\n\n'
    model_eval_text += 'Min prediction time: ' + str(min(pred_durations)) + '\n\n\n\n\n\n'


    with open('model_pred_timings_bert.txt', 'w') as in_file:
        in_file.write(model_eval_text)
    # timings = list()
    # for i in range(100):
    #     time_start = time.time()
    #     with open('clf_pipeline', 'rb') as out_file:
    #         model = pickle.load(out_file)
    #     time_end = time.time()
    #     duration = time_end - time_start
    #     timings.append(duration)
    #
    # pred_time = list()
    # for index, text, label in data.itertuples():
    #     time_start = time.time()
    #     serve_prediction(text, [], model, 'log')
    #     time_end = time.time()
    #     duration = time_end - time_start
    #     pred_time.append(duration)
    #
    #
    # loading_durations = np.array(timings)
    # pred_durations = np.array(pred_time)
    # model_eval_text = str()
    # model_eval_text += 'Logistic Regression Eval\n'
    # model_eval_text += 'Mean duration of loading: ' + str(np.mean(loading_durations)) + '\n\n'
    # model_eval_text += 'Standard deviation of loading: ' + str(np.std(loading_durations)) + '\n\n'
    # model_eval_text += 'Max loading time: ' + str(max(loading_durations)) + '\n\n'
    # model_eval_text += 'Min loading time: ' + str(min(loading_durations)) + '\n\n\n\n'
    # model_eval_text += 'Number of predictions: ' + str(data_length) +'\n'
    # model_eval_text += 'Mean duration of prediction: ' + str(np.mean(pred_durations)) +'\n'
    # model_eval_text += 'Mean duration of prediction: ' + str(np.mean(pred_durations)) + '\n\n'
    # model_eval_text += 'Standard deviation of prediction: ' + str(np.std(pred_durations)) + '\n\n'
    # model_eval_text += 'Max prediction time: ' + str(max(pred_durations)) + '\n\n'
    # model_eval_text += 'Min prediction time: ' + str(min(pred_durations)) + '\n\n\n\n\n\n'
    #
    #
    # with open('model_timings_logistic.txt', 'w') as in_file:
    #     in_file.write(model_eval_text)

if __name__ == '__main__':
    main()