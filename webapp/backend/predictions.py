import pickle
import spacy
import time
import torch
import io
from simpletransformers.classification import ClassificationModel
import os
os.environ["TOKENIZERS_PARALLELISM"] = "false"

nlp = spacy.load('en_core_web_sm')
model = ClassificationModel('bert', 'outputs/checkpoint-4288-epoch-4', num_labels=6, use_cuda=False)


def cleanup_text(text, stopwords):

    text = text.replace('\n', '')
    text = text.replace('\r', '')
    text = text.replace('\t', '')

    text = text.lower() if type(text) == str else text

    text = text.replace('  ', ' ')

    # Remove stop words
    text = ' '.join([word for word in text.split() if stopwords.vocab[word].is_stop == False])

    return text

def serve_prediction(text, stopwords, model, model_type='log'):
    label_dict = {0: 'restaurant', 1: 'movie', 2: 'pizza', 3: 'coffee', 4: 'auto', 5: 'rideshare'}



    if model_type == 'log':
        raw_pred = model.predict([text])
        prediction = label_dict[raw_pred[0]]

    elif model_type == 'bert':
        convo = cleanup_text(text, stopwords)
        raw_pred = model.predict([convo])
        prediction = label_dict[raw_pred[0][0]]

    return prediction

# time_start = time.time()
# serve_prediction(
#     "Hi, I'm looking to book a table for Korean food. \n Ok, what area are you thinking about?\nSomewhere in Southern NYC, maybe the East Village?\nOk, great.  There's Thursday Kitchen, it has great reviews.\nThat's great. So I need a table for tonight at 7 pm for 8 people. We don't want to sit at the bar, but anywhere else is fine. \nThey don't have any availability for 7 pm. \nWhat times are available? \n 5 or 8. \n Yikes, we can't do those times. \n Ok, do you have a second choice? \n Let me check. \n Ok. \n Lets try Boka, are they free for 8 people at 7? \n Yes. \n Great, let's book that. \n Ok great, are there any other requests? \n No, that's it, just book. \n Great, should I use your account you have open with them? \n Yes please.\n Great. You will get a confirmation to your phone soon."
# )
# time_end = time.time()
# duration = time_end - time_start
# print(duration)
if __name__ == "__main__":
    time_start = time.time()
    pred = serve_prediction(
        "pizza restaurants that sell coffee", nlp, model 
    )
    time_end = time.time()
    duration = time_end - time_start
    print("Predicted category = " + pred)
    print('Prediction took: ' + str(duration) + ' seconds to serve.')
