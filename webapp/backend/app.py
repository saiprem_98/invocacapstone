import datetime
import os
import time
# import spacy
# from simpletransformers.classification import ClassificationModel
import pymongo
from pymongo import MongoClient
from flask import Flask, Response, request, render_template, g, redirect, url_for
from flask_mongoengine import MongoEngine
from predictions import  serve_prediction
from sklearn.pipeline import Pipeline
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
os.environ["TOKENIZERS_PARALLELISM"] = "false"


with open('clf_pipeline', 'rb') as out_file:
  pipe = pickle.load(out_file)

app = Flask(__name__)


# This code is meant to load the langauge models into the app context
# Not sure exactly where it should be run though.
# def get_nlp():
#     """load the models into the app context"""

#     if 'nlp' not in g:
#         g.nlp = spacy.load("en_core_web_sm", parser=False, entity=False)

#     return g.nlp

# def get_class_model():

#     if 'class_model' not in g:

#         g.class_model = ClassificationModel('bert', 'outputs/checkpoint-4288-epoch-4', num_labels=6, use_cuda=False)

#     return g.class_model

# def get_db():

#     if 'cluster' not in g:
#         g.cluster = MongoClient("mongodb+srv://sai:kathika@cluster0.gknvq.mongodb.net/dialogues?retryWrites=true&w=majority")

#     if 'db' not in g:
#         g.db = g.cluster["conversation"]

#     if 'collection' not in g:
#         g.collection = g.db["dialogues"]

#     return g.cluster, g.db, g.collection

def connect_mongoDB():
    # cluster, db, collection = get_db()
    cluster = MongoClient("mongodb+srv://sai:kathika@cluster0.gknvq.mongodb.net/dialogues?retryWrites=true&w=majority", serverSelectionTimeoutMS=6000 )                    
    # cluster = MongoClient("mongodb://sai:mongoDB98!@cluster0-shard-00-00.gknvq.mongodb.net:27017,cluster0-shard-00-01.gknvq.mongodb.net:27017,cluster0-shard-00-02.gknvq.mongodb.net:27017/dialogues?ssl=true&replicaSet=atlas-r1krxs-shard-0&authSource=admin&retryWrites=true&w=majority")
    db = cluster["conversation"]
    collection = db["dialogues"]

    user_data = []
    assistant_data = []
    messageHistory = []

    queryUser= {"name": "user"}
    queryAssistant= {"name": "assistant"}
    queryLeftOver = {"name":"speaker"}

    # for x in collection.find({ "name": "user"}):
    #     if(' ' + x['message'] == "STOP" or ' ' + x['message'] == "END"):
    #         break
    #     user_data.append(' ' + x['message'])
    
    # for x in collection.find({ "name": "assistant"}):
    #     if(' ' + x['message'] == "STOP" or ' ' + x['message'] == "END"):
    #         break
    #     assistant_data.append(' ' + x['message'])
    # conversation for both USER and Assistant stored in messageHistory
    messageHistoryStr = str("")
    for x in collection.find():
        if(' ' + x['message'] == "STOP" or ' ' + x['message'] == "END"):
            break
        messageHistoryStr += ' ' + x['message']
        messageHistory.append(' ' + x['message'])
    print(messageHistoryStr)
    
    deleteUser = collection.delete_many(queryUser)
    print(deleteUser.deleted_count, " User documents deleted.")

    deleteAssistant = collection.delete_many(queryAssistant)
    print(deleteAssistant.deleted_count, " Assistant documents deleted.")

    deleteLeftOver = collection.delete_many(queryLeftOver)
    print(deleteLeftOver.deleted_count, " Left Over documents deleted.")
    return messageHistoryStr

@app.route("/api")

def index():
    # time.sleep(35)
    # connect_mongoDB()
    print("Hello World Flask with mongoDB connected")
    return predict()


@app.route('/pred_loop')
def predict():

    # Load language models 
    # stopwords = get_nlp()
    # model = get_class_model()

    messageHistory = connect_mongoDB()

    print(pipe.predict([messageHistory]))

    prediction = serve_prediction([messageHistory], [], pipe)
    print("***********This is the prediction********* ", prediction)
    print(url_for('serve_assisstant', pred_category=prediction))
    return serve_assisstant(prediction)
# The route below will be connected to the preditction function
# It will route the user to the relevant html page
@app.route('/categories/<string:pred_category>')
def serve_assisstant(pred_category):
    print("ENTERED serve_assisstant : ", pred_category)
    # Austin add template paths to this mapping (Done)
    category_mappings = {
            'restaurant': '/restaurant/restaurantpageindex.html',
            'movie': '/movie/moviepageindex.html',
            'pizza': '/pizza/pizzapageindex.html',
            'coffee': '/coffee/coffeepageindex.html',
            'auto': '/autorepair/autorepairpageindex.html',
            'rideshare': '/rideshare/ridesharepageindex.html'
        }
    template_path = category_mappings[pred_category]
    return render_template(template_path) # Variables could be added later




if __name__ == "__main__":
    app.debug = True


    app.run(debug=True, port=5001)
