const http = require("http");
const express = require("express");
const socketio = require("socket.io");
const cors = require("cors");
const { MongoClient } = require("mongodb");

const { addUser, removeUser, getUser, getUsersInRoom } = require("./users");

const router = require("./router");
const uri =
  "mongodb+srv://sai:kathika@cluster0.gknvq.mongodb.net/test?retryWrites=true&w=majority";
const client = new MongoClient(uri);

var chatHistory = [];

async function createMultipleListings(client, newListings) {
  const result = await client
    .db("conversation_history")
    .collection("dialogues_history")
    .insert({ newListings });
  console.log(
    `${result.insertedCount} new listing(s) created with the following id(s):`
  );
  console.log(result.insertedIds);
}

async function createListing(client, newListing) {
  const result = await client
    .db("conversation")
    .collection("dialogues")
    .insertOne(newListing);
  console.log(
    `New listing created with the following id: ${result.insertedId}`
  );
}

async function sendMessages(name, message) {
  try {
    // Connect to the MongoDB cluster
    await client.connect();
    // Make the appropriate DB calls

    await createListing(client, {
      name: name,
      message: message
    });
  } catch (e) {
    console.error(e);
  } finally {
    console.log(
      "************************************** HERE **************************************"
    );
    // await client.close();
  }
}

const app = express();
const server = http.createServer(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*"
  }
});
const PORT = process.env.PORT || 5000;

app.use(cors());
app.use(router);

io.on("connect", socket => {
  socket.on("join", ({ name, room }, callback) => {
    const { error, user } = addUser({ id: socket.id, name, room });

    if (error) return callback(error);

    socket.join(user.room);

    socket.emit("message", {
      user: "admin",
      text: `${user.name}, welcome to room ${user.room}.`
    });
    socket.broadcast
      .to(user.room)
      .emit("message", { user: "admin", text: `${user.name} has joined!` });

    io.to(user.room).emit("roomData", {
      room: user.room,
      users: getUsersInRoom(user.room)
    });

    callback();
  });

  socket.on("sendMessage", (message, callback) => {
    const user = getUser(socket.id);

    io.to(user.room).emit("message", { user: user.name, text: message });
    console.log(
      "************************************** HERE Send Messages **************************************"
    );
    sendMessages(user.name, message);
    var messageObj = { name: user.name, message: message };
    chatHistory.push(messageObj);
    callback();
  });

  socket.on("disconnect", () => {
    const app = express();
    const user = removeUser(socket.id);
    console.log(
      "********************************** HERE Disconnect **************************************"
    );
    console.log(chatHistory);
    // createMultipleListings(client, chatHistory);
    createMultipleListings(client, chatHistory);

    if (user) {
      io.to(user.room).emit("message", {
        user: "Admin",
        text: `${user.name} has left Open http://localhost:5001/api for prediction`
      });
      io.to(user.room).emit("roomData", {
        room: user.room,
        users: getUsersInRoom(user.room)
      });
    }
    app.get("/from", (req, res) => {
      console.log("****** HERE ******");
      res.redirect("http://localhost:5001/api");
    });
    client.close();
  });
});

// server.listen(process.env.PORT || 5000, () => console.log(`Server has started.`));
server.listen(process.env.PORT || 5000, () =>
  console.log(`Server has started.`)
);
